import React, { Component } from 'react'
import Product from "./Product";
import Title from "./Title"
import { ProductConsumer } from "../context"
import { storeProducts } from '../data';

export default class extends Component {
  
  render() {
    /** First basic use */
    // let firstProduct = <Product product={storeProducts[0]} />;
    // let secondProduct = <Product product={storeProducts[1]} />;

    
    /** Render all using For (Basic) */
    let productContainer = [];
    for (let index = 0; index < storeProducts.length; index++) {
      const product = storeProducts[index];

      let productItem = <Product key={index} product={product} />;
      productContainer.push( productItem )
    }

    
    /** Render all using map */
    // let productContainer = storeProducts.map((product, iterationOrder)=>{
    //   return <Product  key={iterationOrder} product={product} />;
    // })

    


    return (
      <React.Fragment>
        <div className="py-5">
          <div className="container">
            <Title name="our" title="product" />
            <div className="row">
              { productContainer }
              {/* <ProductConsumer>
                {value => {
                  return value.products.map(product => {
                    debugger
                    return <Product key={product.id} product={product} />
                  })
                }}
              </ProductConsumer> */}

            </div>

          </div>

        </div>
      </React.Fragment>
    )
  }
}
